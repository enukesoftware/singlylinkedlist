/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singlylinkedlistapplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author enuke
 */
class node {

    int data;
    node prev, next;

    public node(int x) {
        data = x;
        next = null;
    }
}

class SLL {

    node start = null;
    
    protected node end;


    boolean isEmpty() {

        return start == null;

    }

    public int removeLast() {
        if (isEmpty()) {
            System.out.println("empty");
            return 0;
        } else {
            node current = start;
            while (current.next.next != null) {
                current = current.next;
            }
            int x = current.next.data;
            current.next = null;
            return x;
        }
    }

    public void removeAllBasedOnInputValue(int val) {

        if (isEmpty()) {
            System.out.println("empty");
        } else {

            int counter = 0;
            node current = start;

            //here we will go to the last node
            while (current.next != null) {
                if (current.data > val) {
                    /* Here, we need to verify 3 things:
                * 1 - If it is the start;
                * 2 - If it is the end; and
                * 3 - If it is the body.
                     */

 /*1st verification - 
                If the start is bigger than your value,
                then you just make your next node as "start",
                and make its previous as NULL.*/
                    if (current == start) {
                        start = current.next;
                        current.next.prev = null;
                    }/*2nd verification - 
                If it is the last element,
                then you just make the next node of your previous be NULL.*/ else if (current.next == null) {
                        current.prev.next = null;
                    }/*3rd verification - 
                You will make the next of the previous as your current next; 
                and the previous of the next as your current previous. 
                In that way you will lose all the ways of reaching the current 
                (which is greater than the value)*/ else {
                        current.prev.next = current.next;
                        current.next.prev = current.prev;
                    }
                }
                current.next = current.next;
            }

        }
    }

    public void display() {
        // if (isEmpty())
        System.out.println("The list is empty");
//   else
//   {
        node current = start;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
    }
    
     public void insertAtEnd(int val) {

        node nptr = new node(val);


        if (start == null) {

            start = nptr;

            end = start;

        } else {

            end.next=nptr;

            end = nptr;
        }

    }

}

public class NewClass {

    public static void main(String[] args) throws IOException {
        InputStreamReader obj = new InputStreamReader(System.in);
        BufferedReader r = new BufferedReader(obj);
        int ch;
        SLL s = new SLL();
        do {
            System.out.println("1.Remove");
            System.out.println("2.Display");
            System.out.println("3.Exit");
            System.out.println("Enter your choice:");
            ch = Integer.parseInt(r.readLine());
            switch (ch) {

                case 1:
                    System.out.println("1.Append List");
                    System.out.println("2.Remove tail");
                    System.out.println("3.Remove all elements based on specific value");
                    System.out.println("Enter choice:");
                    int al1 = Integer.parseInt(r.readLine());
                    switch (al1) {

                        case 1:
                           s.insertAtEnd(2);
                            break;
                        case 2:
                            s.removeLast();
                            break;
                        case 3:
                            s.removeAllBasedOnInputValue(3);
                            break;

                    }
                    break;

                case 2:
                    s.display();
                    break;
                case 3:
                    break;
            }
        } while (ch != 3);

    }
}
