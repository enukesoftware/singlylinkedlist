/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singlylinkedlistapplication;

import java.util.Scanner;

/**
 *
 * @author enuke
 */
public class SinglyLinkedListApplication {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);

        /* Creating object of class linkedList */
        LinkedList list = new LinkedList();

        System.out.println("Singly Linked List Test\n");

        char ch;

        /*  Perform list operations  */
        do {
            
            

            System.out.println("\nSingly Linked List Operations\n");

            System.out.println("1. insert at begining");

            System.out.println("2. Append Linked List");

            System.out.println("3. Remove the tail element from a linkedlist");

            System.out.println("4. Remove all element in the linkedlist that is great than a target value");

            System.out.print("Enter your choice :");

            int choice = scan.nextInt();

            switch (choice) {

                case 1:

                    System.out.println("Enter integer element to insert");

                    list.insertAtStart(scan.nextInt());

                    break;

                case 2:

                    System.out.println("Enter integer element to insert");

                    list.insertAtEnd(scan.nextInt());

                    break;

                case 3:

                    int pos = list.getSize();

                    if (pos < 1) {
                        System.out.println("Invalid position\n");
                    } else {
                        list.deleteAtPos(pos);
                    }

                    break;

                case 4:

                    System.out.println("Enter target Value");

                    int p = scan.nextInt();

                    list.deleteGreaterThanTargetNode(p);

                    break;

                default:

                    System.out.println("Wrong Entry \n ");

                    break;

            }

            /*  Display List  */
            list.display();

            System.out.println("\nDo you want to continue (Type y or n) \n");

            ch = scan.next().charAt(0);

        } while (ch == 'Y' || ch == 'y');
    }

}
