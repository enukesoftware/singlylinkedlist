/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singlylinkedlistapplication;

/**
 *
 * @author enuke
 * // Program to implement Singly LinkedList.
 * This class represents the node.
 * Node has a data as int value, and a reference to the other node.
 */
public class Node {
    
    // data as int
    protected int data;

    // next node
    protected Node link;
    
    // previous link;
    protected Node pre;

 
    /*  Constructor  */
// default donstructor
    public Node()

    {

        // initially link is null
        link = null;

        // data value is 0 by default
        data = 0;

    }    

    /*  Constructor  */
// Parameterize constructor for data and node
    public Node(int d,Node n)

    {

        data = d;

        link = n;

    }    

    /*  Function to set link to next Node  */

    public void setLink(Node n)

    {

        link = n;

    }    

      /*  Function to set link to previous Node  */

    public void setPre(Node n)

    {

        pre = n;

    }    

    /*  Function to set data to current Node  */

    public void setData(int d)

    {

        data = d;

    }    

    /*  Function to get link to next node  */

    public Node getLink()

    {

        return link;

    }    
    
     /*  Function to get link to next node  */

    public Node getPre()

    {

        return pre;

    }    


    /*  Function to get data from current Node  */

    public int getData()

    {

        return data;

    }
}
